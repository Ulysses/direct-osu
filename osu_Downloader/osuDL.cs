﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading;
using System.Runtime.InteropServices;
using VInjDn;

namespace osu_Downloader
{
    public partial class mainForm : Form
    {
        Monitor _monitor = new Monitor();
        Dictionary<string, string> urls = new Dictionary<string, string>();
        Dictionary<string, string> churls = new Dictionary<string, string>();

        Process bhv;
        List<System.Diagnostics.Process> handles = new List<Process>();
        int countHandles;
        int changed = 1;
        int listenTime = 0;
        string tempUrl = Clipboard.GetText();
        string[] filt = { "\r\n" };        
        DateTime _dt = DateTime.Now;
        
        /// <summary>
        /// Hook注入osu
        /// </summary>
        /// <returns></returns>
        public bool Inception()
        {
            string arg = "";
            if (Process.GetProcessesByName("osu!").Length <= 0 && Core._osupath != null)
            {
                arg = " -osu";
            }
            if (Core._hooked == true && Core._disableHook == false)
            {
                txt.AppendText(Environment.NewLine + "Already hooked!");
                return true;
            }

            txt.AppendText( Environment.NewLine + "Start Hooking...direct!osu will restart.");            
            Process hookedDirect = Process.Start(Core._directPath,arg);
            Process nowDirect = Process.GetCurrentProcess();
            nowDirect.CloseMainWindow();
            return true;
        }

        public mainForm()
        {
            InitializeComponent();
        }
        public mainForm(bool hooked)
        {
            InitializeComponent();
            if (hooked == true)
            {
                lbl_hooked.Text = "Hook模式已启用";
                lbl_hooked.ForeColor = Color.Red;
                Core._hooked = true;
                Core._disableHook = false;
            }
        }

        private void mainForm_Load(object sender, EventArgs e)
        {             
            Version ver = System.Environment.OSVersion.Version;
            //判断是不是Vista及更新的系统
            if (ver.Major >= 6 && ver.Minor >= 0)
            {
                this.Icon = Properties.Resources.menu_osu;
            }
            else //xp以及更老的系统
            {
                this.Icon = Properties.Resources.menu_osu48;
            }
            Core.InitCore(txt, notifyIco, chk_autoReturn, pbr_download, list_down);
            timer1.Enabled = true;
            File.Delete("log.txt");
            if (Properties.Settings.Default._hookType == 1)
            {
                rdo_CPHook.Checked = true;
            }
            else if (Properties.Settings.Default._hookType == 2)
            {
                rdo_SEHook.Checked = true;
            }
            else
            {
                rdo_ALLHook.Checked = true;
            }
            if (rdo_disable.Checked)
            {
                timer1.Stop();
            }
            if (osu.FindOsuDefault(ref Core._osupath, lbl_path, txt, ref Core._downloadedmap.setIDs, ref Core._downloadedmap.names))
            {
                btn_Chooseosu.Enabled = false;
            }
            this.Activate();
        }



        private void timer1_Tick(object sender, EventArgs e)
        {
            if (rdo_auto.Checked)
            {
                GetBrowserProgress();
                if (!backgroundWorker1.IsBusy && handles.Count != countHandles)
                {
                    backgroundWorker1.RunWorkerAsync();
                    listenTime++;
                    if (listenTime > 15)
                    {
                        countHandles = handles.Count;
                        listenTime = 0;
                    }
                }
                Listen();
            }
            if (rdo_manual.Checked && Clipboard.ContainsText() && Clipboard.GetText() != tempUrl)
            {
                if (Clipboard.GetText().Contains("osu.ppy.sh/b/") || Clipboard.GetText().Contains("osu.ppy.sh/s/"))
                {
                    Core.ShowURL(Clipboard.GetText());
                    tempUrl = Clipboard.GetText();
                }
                else if (Clipboard.GetText().Contains("bloodcat.com/osu/m/"))
                {
                    txt.AppendText( Environment.NewLine + Clipboard.GetText());
                    string number = Clipboard.GetText().Remove(0, Clipboard.GetText().LastIndexOf('/') + 1);
                    DirectDownload(number);
                    Core._tipQ.Enqueue(new Core.Notify(1, "bloodcat编号s" + number));
                    Core.ShowNotify();
                    tempUrl = Clipboard.GetText();
                }
            }
        }

        /// <summary>
        /// 直接下载
        /// </summary>
        /// <param name="number">SetID</param>
        private void DirectDownload(string number)
        {
            Thread th = new Thread(Core.sync);
            if (Core._tDownload.Keys.Contains(number))
            {
                Core._tDownload[number].Abort();
                Core._tDownload[number].Join();
                Core._tDownload.Remove(number);
            }
            Core._tDownload.Add(number, th);
            if (list_down.FindItemWithText(number) == null)
            {
                list_down.Items.Add(new ListViewItem(new string[] { "m/" + number, "0%", "", number, "" })).Selected = true;
            }
            th.Start("m/" + number);
        }

        private void GetBrowserProgress()
        {
            handles.Clear();
            handles.AddRange(System.Diagnostics.Process.GetProcessesByName("chrome"));
            handles.AddRange(System.Diagnostics.Process.GetProcessesByName("Firefox"));
            handles.AddRange(System.Diagnostics.Process.GetProcessesByName("360chrome"));
            handles.AddRange(System.Diagnostics.Process.GetProcessesByName("360se"));
        }

        /// <summary>
        /// 监听IE并准备下载
        /// </summary>
        private void Listen()
        {
            urls.AddRange(Monitor.monitorUrl(), true);

            foreach (string url in urls.Keys)
            {
                if (url == null)
                {
                    continue;
                }
                if (url.Contains("osu.ppy.sh/b/") || url.Contains("osu.ppy.sh/s/"))
                {
                    if (Monitor.judgeUrl(url))
                    {
                        Core.ShowURL(url, urls[url]);
                    }
                }
            }
            if (urls.Count > 0)
            {
                tempUrl = urls.Last().Key;
            }
            urls.Clear();
        }
        public static void DownloadURL(string url)
        {
        }

        /// <summary>
        /// 监听非IE浏览器并下载
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            if (bhv != null)
            {
                if (!bhv.HasExited)
                {
                    return;
                }
            }
            DateTime dt = DateTime.Now;
            string str;
            string[] strcut;
            string[] url_name = new string[2];
            string startBHV = String.Format("{0}\\BHV.exe", Application.StartupPath);
            string BHVfilter = string.Format("/scomma log.txt /sort \"~Visit Time\" /HistorySource 2 /VisitTimeFilterType 4 /VisitTimeFrom \"{0}\" /VisitTimeTo \"{1}\" /LoadIE 0", _dt.AddSeconds(-30).ToUniversalTime().ToString("dd-MM-yyyy HH:mm:ss"), dt.AddMinutes(10).ToUniversalTime().ToString("dd-MM-yyyy HH:mm:ss"));
            try
            {
                bhv = Process.Start(startBHV, BHVfilter);
                if (File.Exists("log.txt"))
                {
                    str = File.ReadAllText("log.txt", Encoding.ASCII);
                    str = str.Remove(0, str.IndexOf("\r\n"));
                    strcut = str.Split(filt, StringSplitOptions.RemoveEmptyEntries);
                    foreach (string urlandname in strcut)
                    {
                        if (urlandname.Contains("osu.ppy.sh/b/") || urlandname.Contains("osu.ppy.sh/s/"))
                        {
                            url_name = urlandname.Split(',');
                            if (Monitor.judgeUrl(url_name[0]))
                            {
                                if (churls.ContainsKey(url_name[0]))
                                {
                                    continue;
                                }
                                changed++;
                                if (changed >= 100)
                                {
                                    changed = 1;
                                }
                                churls.Add(url_name[0], url_name[1]);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                foreach (string url in churls.Keys)
                {
                    Core.ShowURL(url, churls[url]);
                }
                churls.Clear();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }
        /// <summary>
        /// 下载完毕文本框提示（已废弃）
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void txtUpdate(object sender, EventArgs e)
        {
            Core._txt.AppendText(Environment.NewLine + (string)sender + "下载完毕");
        }


        private void btn_Chooseosu_Click(object sender, EventArgs e)
        {
            if (osu.Setpath(ref Core._osupath))
            {
                btn_Chooseosu.Enabled = false;
                lbl_path.Text = "当前目录：" + Core._osupath;
                if (Core._downloadedmap.setIDs.Count == 0)
                {
                    Core._downloadedmap.setIDs.AddRange(osu.GetExistMaps(Core._osupath, ref Core._downloadedmap.names));
                }
                txt.AppendText( Environment.NewLine + "已扫描到" + Core._downloadedmap.setIDs.Count() + "个曲目");
            }
        }



        private void btn_hook_Click(object sender, EventArgs e)
        {
            Core._disableHook = false;
            if (Inception())
            {
                rdo_disable.Checked = true;
                lbl_hooked.Text = "Hook模式已启用";
                lbl_hooked.ForeColor = Color.Red;
            }
        }
        #region 窗体事件
        private void rdo_auto_CheckedChanged(object sender, EventArgs e)
        {
            if (rdo_auto.Checked || rdo_manual.Checked)
            {
                timer1.Start();
            }
            else
            {
                timer1.Stop();
            }
        }

        private void notifyIcon1_BalloonTipShown(object sender, EventArgs e)
        {
            Core._isShowingTip = true;
        }

        private void notifyIcon1_BalloonTipClosed(object sender, EventArgs e)
        {
            Core._isShowingTip = false;            
            if (Core._tipQ.Count > 0)
            {
                Core.ShowNotify();
            }
        }

        private void chk_autoReturn_CheckedChanged(object sender, EventArgs e)
        {
            if (chk_autoReturn.Checked)
            {
                if (Process.GetProcessesByName("osu!").Length <= 0)
                {
                    chk_autoReturn.Checked = false;
                }
            }
        }
        private void mainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            //if (ip != null)
            //{
            //    ip.Eject(Path.GetFileNameWithoutExtension(Application.ExecutablePath) + ".Main");
            //}
            Core._disableHook = true;
            foreach (Thread th in Core._tDownload.Values)
            {
                th.Abort();
                th.Join();
            }
            Core._notifyIco.Visible = false;
            if (Core._syncQQ)
            {
                QQ.Send2QQ(Core._QQ, "");
                Core._syncQQ = false;
            }
            Application.ExitThread();
            GC.Collect();
            if (Core._hooked == false)
            {
                Application.Exit();
            }
            if (Program.ip != null)
            {
                MessageBox.Show("eject");
                Program.ip.Eject("osu_Downloader.Main");
            }
        }
        

        private void btn_unhook_Click(object sender, EventArgs e)
        {
            Core._disableHook = true;
            lbl_hooked.Text = "Hook模式未启用";
            lbl_hooked.ForeColor = Color.Black;
            //if (!Core._hooked && Process.GetProcessesByName("osu!").Count() > 0)
            //{
            //    Program.ip.Eject("osu_Downloader.Main");
            //}
            //if (ip != null)
            //{
            //    ip.Eject(Path.GetFileNameWithoutExtension(Application.ExecutablePath) + ".Main");
            //}
        }

        private void rdo_SEHook_CheckedChanged(object sender, EventArgs e)
        {
            if (rdo_CPHook.Checked)
            {
                Properties.Settings.Default._hookType = 1;
            }
            if (rdo_SEHook.Checked)
            {
                Properties.Settings.Default._hookType = 2;
            }
            if (rdo_ALLHook.Checked)
            {
                Properties.Settings.Default._hookType = 0;
            }
            Properties.Settings.Default.Save();
        }

        private void mStop_Click(object sender, EventArgs e)
        {
            if (list_down.SelectedItems.Count > 0)
            {
                Core._tDownload[list_down.SelectedItems[0].SubItems[3].Text].Abort();
                //Core._tDownload[list_down.SelectedItems[0].SubItems[3].Text].Join();
                GC.Collect();
                list_down.SelectedItems[0].SubItems[1].Text = "被终止";
            }
        }

        private void mRestart_Click(object sender, EventArgs e)
        {
            mStop_Click(sender, e);
            if (list_down.SelectedItems.Count > 0)
            {
                DirectDownload(list_down.SelectedItems[0].SubItems[3].Text);
            }
        }

        private void txt_LinkClicked(object sender, LinkClickedEventArgs e)
        {
            Process.Start(e.LinkText);
        }

        private void mCopy_Click(object sender, EventArgs e)
        {
            if (list_down.SelectedItems.Count > 0)
            {
                Clipboard.SetText("http://bloodcat.com/osu/" + list_down.SelectedItems[0].SubItems[0].Text);
            }
        }

        private void mInfo_Click(object sender, EventArgs e)
        {
            if (list_down.SelectedItems.Count > 0)
            {
                Process.Start("http://osu.ppy.sh/s/" + list_down.SelectedItems[0].SubItems[3].Text);
            }
        }

        private void list_down_DoubleClick(object sender, EventArgs e)
        {
            if (list_down.SelectedItems.Count > 0)
            {
                Process.Start("http://bloodcat.com/osu/" + list_down.SelectedItems[0].SubItems[0].Text);
            }
        }

        private void txt_TextChanged(object sender, EventArgs e)
        {
            txt.ScrollToCaret();
        }
        #endregion

        private void btn_syncQQ_Click(object sender, EventArgs e)
        {
            if (Process.GetProcessesByName("osu!").Count() <=0)
            {
                MessageBox.Show("需要先打开osu才能使用同步功能~", "direct!music");
            }
            else
            {
                (new SyncToQQ()).Show();
            }
        }
        private void btn_about_Click(object sender, EventArgs e)
        {  
            (new About()).ShowDialog();
        }

        private void pic_direct_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                if (Process.GetProcessesByName("osu!").Count() <= 0 && Core._osupath != null && Core._osupath != "")
                {
                    Process.Start(Core._osupath + "\\osu!.exe");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        private void mOgl_Click(object sender, EventArgs e)
        {
            if (list_down.SelectedItems.Count > 0)
            {
                Process.Start("http://osu.uu.gl/s/" + list_down.SelectedItems[0].SubItems[3].Text);
            }
        }

        private void mYas_Click(object sender, EventArgs e)
        {
            if (list_down.SelectedItems.Count > 0)
            {
                Process.Start("http://osu.yas-online.net/m#" + list_down.SelectedItems[0].SubItems[3].Text);
            }
        }

        private void lbl_path_TextChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default._savedPath = Core._osupath;
            Properties.Settings.Default.Save();
        }

        private void lbl_path_Click(object sender, EventArgs e)
        {

        }
    }
}
