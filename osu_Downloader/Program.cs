﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using VInjDn;
using System.Diagnostics;
using System.IO;
using System.Threading;

namespace osu_Downloader
{
    static class Program
    {
        public static InjectableProcess ip;
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            bool _hooked = false;
            bool _inject = true;
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            Core._directPath = Application.ExecutablePath;
            Core._directStartupPath = Application.StartupPath;
            if (Core._directStartupPath == "" || Core._directStartupPath == null)
            {
                Core._directStartupPath = Properties.Settings.Default._savedPath;
            }
            if (Core._directStartupPath == "" || Core._directStartupPath == null)
            {
                Core._directStartupPath = "D:\\";
            }            
            Core._QQ = Properties.Settings.Default._savedQQ;
            Core._syncQQ = Properties.Settings.Default._syncQQ;
            ExactLibs();
            if (args.Length > 0)
            {
                foreach (string arg in args)
                {
                    if (arg.Trim() == "-nohook")
                    {
                        _inject = false;
                    }
                    if (arg.Trim() == "-cphook")
                    {
                        Properties.Settings.Default._hookType = 1;
                    }
                    if (arg.Trim() == "-sehook")
                    {
                        Properties.Settings.Default._hookType = 2;
                    }
                    if (arg.Trim() == "-osu")
                    {
                        if (Process.GetProcessesByName("osu!").Length <= 0 && Properties.Settings.Default._savedPath != null)
                        {
                            Process.Start(Properties.Settings.Default._savedPath + "\\osu!.exe");
                        }
                    }
                }
            }
            _hooked = Injection(_inject);
            if (_hooked == false)
            {
                Core._directPath = Application.ExecutablePath;
                Core._directStartupPath = Application.StartupPath;
                Application.Run(new mainForm());
            }
        }

        /// <summary>
        /// 解压将要用到的东西
        /// </summary>
        private static void ExactLibs()
        {
            if (!File.Exists(Core._directStartupPath + "\\EasyHook32.dll") || !File.Exists(Core._directStartupPath + "\\VInj.dll") || !File.Exists(Core._directStartupPath + "\\BHV.exe"))
            {
                byte[] buffer = Properties.Resources.BHV;//这个是添加EXE到程序资源时的名称
                FileStream FS = new FileStream(Core._directStartupPath + "\\BHV.exe", FileMode.Create);//新建文件
                BinaryWriter BWriter = new BinaryWriter(FS);//以二进制打开文件流
                BWriter.Write(buffer, 0, buffer.Length);//从资源文件读取文件内容，写入到一个文件中
                BWriter.Close();
                FS.Close();
                buffer = Properties.Resources.BHVcfg;
                FS = new FileStream(Core._directStartupPath + "\\BHV.cfg", FileMode.Create);
                BWriter = new BinaryWriter(FS);
                BWriter.Write(buffer, 0, buffer.Length);
                BWriter.Close();
                FS.Close();
                buffer = Properties.Resources.EasyHook;
                FS = new FileStream(Core._directStartupPath + "\\EasyHook.dll", FileMode.Create);
                BWriter = new BinaryWriter(FS);
                BWriter.Write(buffer, 0, buffer.Length);
                BWriter.Close();
                FS.Close();
                buffer = Properties.Resources.EasyHook32;
                FS = new FileStream(Core._directStartupPath + "\\EasyHook32.dll", FileMode.Create);
                BWriter = new BinaryWriter(FS);
                BWriter.Write(buffer, 0, buffer.Length);
                BWriter.Close();
                FS.Close();
                buffer = Properties.Resources.VInj;
                FS = new FileStream(Core._directStartupPath + "\\VInj.dll", FileMode.Create);
                BWriter = new BinaryWriter(FS);
                BWriter.Write(buffer, 0, buffer.Length);
                BWriter.Close();
                FS.Close();
                buffer = Properties.Resources.VInjDn;
                FS = new FileStream(Core._directStartupPath + "\\VInjDn.dll", FileMode.Create);
                BWriter = new BinaryWriter(FS);
                BWriter.Write(buffer, 0, buffer.Length);
                BWriter.Close();
                FS.Close();
                buffer = Properties.Resources.Interop_SHDocVw;
                FS = new FileStream(Core._directStartupPath + "\\Interop.SHDocVw.dll", FileMode.Create);
                BWriter = new BinaryWriter(FS);
                BWriter.Write(buffer, 0, buffer.Length);
                BWriter.Close();
                FS.Close();
            }

        }

        private static bool Injection(bool inject = true)
        {
            if (!inject)
            {
                return false;
            }
            bool hooked = false;
            if (Process.GetProcessesByName("osu!").Length > 0)
            {
                try
                {
                    ip = InjectableProcess.Create(Process.GetProcessesByName("osu!")[0].Handle);
                    int result = ip.Inject(Application.ExecutablePath, "osu_Downloader.Main");

                    if (result == 0)
                    {
                        MessageBox.Show("挂载失败\n此版本的direct!osu只能通过direct!osu启动osu!才能实现挂载。", "direct!fail");
                        Console.WriteLine("Failed to inject.");
                        hooked = false;
                    }
                    else
                    {
                        Console.WriteLine("Hooked to target.");
                        hooked = true;
                    }
                }
                catch (Exception)
                {
                    hooked = false;
                }
            }
            return hooked;
        }
    }
}
