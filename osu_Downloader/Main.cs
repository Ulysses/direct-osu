﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Diagnostics;
using EasyHook;
using VInjDn;
using System.Threading;
using System.Windows.Forms;

namespace osu_Downloader
{
    public class Main : VInjDn.IInjectable
    {
        #region SetWindowText钩子
        [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Unicode)]
        public static extern bool SetWindowTextW(IntPtr hwnd, [MarshalAs(UnmanagedType.LPTStr)]string lpString);

        [UnmanagedFunctionPointer(CallingConvention.StdCall, CharSet = CharSet.Unicode, SetLastError = true)]
        delegate bool DSetWindowText(IntPtr hwnd, String lpString);

        static bool SetWindowText_Hooked(IntPtr hwnd, [MarshalAs(UnmanagedType.LPTStr)] String lpString)
        {
            if (Process.GetProcessesByName("osu!").Count() > 0 && Core._syncQQ)
            {
                //MessageBox.Show(lpString);
                if (hwnd == Process.GetProcessesByName("osu!")[0].MainWindowHandle && lpString.Contains("osu!"))
                {
                    //同步
                    //if ()
                    {
                        string song = lpString.Replace("osu!", "").Trim();
                        if (song == "")
                        {
                            new Thread(WaitForCancel).Start();
                            //QQ.Send2QQ(Core._QQ, "");
                        }
                        else
                        {
                            try
                            {
                                song = song.Remove(0, 2).Trim();
                                Core._txt.AppendText(Environment.NewLine + song);
                                if (!Properties.Settings.Default._showDiff)
                                {
                                    if (song.Contains('['))
                                    {
                                        song = song.Remove(song.LastIndexOf('['));
                                    }
                                }
                                if (Properties.Settings.Default._tryName)
                                {
                                    string artist;
                                    string realName = osu.GetSongName(song, out artist, Properties.Settings.Default._showDiff);
                                    song = artist + " - " + realName;
                                }
                                QQ.Send2QQ(Core._QQ, song);
                                Core._nowPlaying = song;
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex);
                            }

                        }
                    }
                }
            }
            return SetWindowTextW(hwnd, lpString);
        }
        private static void WaitForCancel()
        {
            Thread.Sleep(1000);
            if (Process.GetProcessesByName("osu!")[0].MainWindowTitle == "osu!")
            {
                QQ.Send2QQ(Core._QQ, "");
            }
        }
        #endregion
        #region CreateProcess钩子
        [UnmanagedFunctionPointer(CallingConvention.StdCall, CharSet = CharSet.Unicode, SetLastError = true)]
        delegate bool DCreateProcess(string lpApplicationName,
           string lpCommandLine, ref SECURITY_ATTRIBUTES lpProcessAttributes,
           ref SECURITY_ATTRIBUTES lpThreadAttributes, bool bInheritHandles,
           uint dwCreationFlags, IntPtr lpEnvironment, string lpCurrentDirectory,
           [In] ref STARTUPINFO lpStartupInfo,
           out PROCESS_INFORMATION lpProcessInformation);

        const uint NORMAL_PRIORITY_CLASS = 0x0020;
        [DllImport("kernel32.dll", SetLastError = true, CharSet = CharSet.Unicode)]
        static extern bool CreateProcess(string lpApplicationName,
           string lpCommandLine, ref SECURITY_ATTRIBUTES lpProcessAttributes,
           ref SECURITY_ATTRIBUTES lpThreadAttributes, bool bInheritHandles,
           uint dwCreationFlags, IntPtr lpEnvironment, string lpCurrentDirectory,
           [In] ref STARTUPINFO lpStartupInfo,
           out PROCESS_INFORMATION lpProcessInformation);
        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
        struct STARTUPINFO
        {
            public Int32 cb;
            public string lpReserved;
            public string lpDesktop;
            public string lpTitle;
            public Int32 dwX;
            public Int32 dwY;
            public Int32 dwXSize;
            public Int32 dwYSize;
            public Int32 dwXCountChars;
            public Int32 dwYCountChars;
            public Int32 dwFillAttribute;
            public Int32 dwFlags;
            public Int16 wShowWindow;
            public Int16 cbReserved2;
            public IntPtr lpReserved2;
            public IntPtr hStdInput;
            public IntPtr hStdOutput;
            public IntPtr hStdError;
        }
        [StructLayout(LayoutKind.Sequential)]
        public struct SECURITY_ATTRIBUTES
        {
            public int nLength;
            public IntPtr lpSecurityDescriptor;
            public int bInheritHandle;
        }
        [StructLayout(LayoutKind.Sequential)]
        internal struct PROCESS_INFORMATION
        {
            public IntPtr hProcess;
            public IntPtr hThread;
            public int dwProcessId;
            public int dwThreadId;
        }

        static bool CreateProcess_Hooked(string lpApplicationName,
           string lpCommandLine, ref SECURITY_ATTRIBUTES lpProcessAttributes,
           ref SECURITY_ATTRIBUTES lpThreadAttributes, bool bInheritHandles,
           uint dwCreationFlags, IntPtr lpEnvironment, string lpCurrentDirectory,
           [In] ref STARTUPINFO lpStartupInfo,
           out PROCESS_INFORMATION lpProcessInformation)
        {
            try
            {
                if (!Core._disableHook && (Properties.Settings.Default._hookType == 0 || Properties.Settings.Default._hookType == 1))
                {
                    //Core._txt.AppendText(Environment.NewLine + "cp " + Properties.Settings.Default._hookType);

                    string url = lpCommandLine.Remove(0, lpCommandLine.IndexOf("--") + 2);
                    url = url.Replace("\"", "").Trim();
                    if (Core._tempOsuUrl != url && (url.Contains("osu.ppy.sh/b/") || url.Contains("osu.ppy.sh/s/")))
                    {
                        Core._tempOsuUrl = url;
                        Core.player.Stream = Properties.Resources.soft_hitclap;
                        Core.player.Play();
                        //MessageBox.Show(url + "\nCatched in CPHook");
                        Core.ShowURL(url);
                        lpProcessInformation = new PROCESS_INFORMATION();
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            // call original API...
            return CreateProcess(
           lpApplicationName,
           lpCommandLine, ref lpProcessAttributes,
           ref lpThreadAttributes, bInheritHandles,
           dwCreationFlags, lpEnvironment, lpCurrentDirectory,
           ref lpStartupInfo,
           out lpProcessInformation);
        }
        #endregion
     
        #region ShellExecuteEx钩子
        public delegate bool DShellExecuteEx(ref SHELLEXECUTEINFO lpExecInfo);
        //[DllImport("shell32.dll", CharSet = CharSet.Auto)]
        //static extern bool ShellExecuteExW(ref SHELLEXECUTEINFO lpExecInfo);
        [DllImport("shell32.dll", CharSet = CharSet.Unicode, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool ShellExecuteExW(ref SHELLEXECUTEINFO lpExecInfo);

        [StructLayout(LayoutKind.Sequential)]
        public struct SHELLEXECUTEINFO
        {
            public int cbSize;
            public uint fMask;
            public IntPtr hwnd;
            [MarshalAs(UnmanagedType.LPTStr)]
            public string lpVerb;
            [MarshalAs(UnmanagedType.LPTStr)]
            public string lpFile;
            [MarshalAs(UnmanagedType.LPTStr)]
            public string lpParameters;
            [MarshalAs(UnmanagedType.LPTStr)]
            public string lpDirectory;
            public int nShow;
            public IntPtr hInstApp;
            public IntPtr lpIDList;
            [MarshalAs(UnmanagedType.LPTStr)]
            public string lpClass;
            public IntPtr hkeyClass;
            public uint dwHotKey;
            public IntPtr hIcon;
            public IntPtr hProcess;
        }

        static bool ShellExecuteEx_Hooked(ref SHELLEXECUTEINFO lpExecInfo)
        {
            try
            {
                if (Core._tempOsuUrl != lpExecInfo.lpFile.Trim() && !Core._disableHook && (Properties.Settings.Default._hookType == 0 || Properties.Settings.Default._hookType == 2))
                {
                    //Core._txt.AppendText(Environment.NewLine+"se " + Properties.Settings.Default._hookType);
                    string url = lpExecInfo.lpFile.Trim();
                    Core._tempOsuUrl = url;
                    //url = url.Replace("\"", "");
                    if (url.Contains("osu.ppy.sh/b/") || url.Contains("osu.ppy.sh/s/"))
                    {
                        Core.player.Stream = Properties.Resources.soft_hitnormal;
                        Core.player.Play();
                        //MessageBox.Show(url + "\nCatched in SEHook");
                        Core.ShowURL(url);
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            //Console.WriteLine("living~");
            //This.Interface.OnCreateProcess("ShellExecuteEx_Hooked:");
            //This.Interface.OnCreateProcess("lpParameters：" + lpExecInfo.lpParameters);
            //This.Interface.OnCreateProcess("lpDirectory：" + lpExecInfo.lpDirectory);
            //This.Interface.OnCreateProcess("lpClass：" + lpExecInfo.lpClass);
            //This.Interface.OnCreateProcess("lpVerb：" + lpExecInfo.lpVerb);
            //This.Interface.OnCreateProcess("lpFile：" + lpExecInfo.lpFile);
            return ShellExecuteExW(ref lpExecInfo);
            //return true;
        }

        public enum ShowCommands : int
        {
            SW_HIDE = 0,
            SW_SHOWNORMAL = 1,
            SW_NORMAL = 1,
            SW_SHOWMINIMIZED = 2,
            SW_SHOWMAXIMIZED = 3,
            SW_MAXIMIZE = 3,
            SW_SHOWNOACTIVATE = 4,
            SW_SHOW = 5,
            SW_MINIMIZE = 6,
            SW_SHOWMINNOACTIVE = 7,
            SW_SHOWNA = 8,
            SW_RESTORE = 9,
            SW_SHOWDEFAULT = 10,
            SW_FORCEMINIMIZE = 11,
            SW_MAX = 11
        }
        #endregion
        LocalHook CreateProcessHook;
        LocalHook ShellExecuteHook;
        LocalHook SetWindowTextHook;
        public int OnCommand(VInjDn.LiquidCommand command)
        {
            return 1;
        }

        public int OnLoad()
        {
            try
            {                
                ShellExecuteHook = LocalHook.Create(LocalHook.GetProcAddress("shell32.dll", "ShellExecuteExW"), new DShellExecuteEx(ShellExecuteEx_Hooked), this);
                ShellExecuteHook.ThreadACL.SetExclusiveACL(new Int32[] { 0 });               
                CreateProcessHook = LocalHook.Create(LocalHook.GetProcAddress("kernel32.dll", "CreateProcessW"), new DCreateProcess(CreateProcess_Hooked), this);
                CreateProcessHook.ThreadACL.SetExclusiveACL(new Int32[1]);
                SetWindowTextHook = LocalHook.Create(LocalHook.GetProcAddress("user32.dll", "SetWindowTextW"), new DSetWindowText(SetWindowText_Hooked), this);
                SetWindowTextHook.ThreadACL.SetExclusiveACL(new Int32[] { 0 });
                
                Thread th = new Thread(EntryThread);
                th.SetApartmentState(ApartmentState.STA);
                th.Start();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            return 1;
        }

        public int OnUnload()
        {
            MessageBox.Show("注入已被卸载.", "direct!fullstop");
            ShellExecuteHook.ThreadACL.SetInclusiveACL(new Int32[1]);
            CreateProcessHook.ThreadACL.SetInclusiveACL(new Int32[1]);
            SetWindowTextHook.ThreadACL.SetInclusiveACL(new Int32[1]);
            Application.ExitThread();
            return 1;
        }
        private void EntryThread()
        {
            Core.player.Stream = Properties.Resources.drum_hitfinish;
            Core.player.Play();
            Properties.Settings.Default._savedPath = Application.StartupPath;
            if (Core._osupath == null || Core._osupath == "")
            {
                Core._osupath = Application.StartupPath;                
                Properties.Settings.Default.Save();
            }
            Core._syncQQ = Properties.Settings.Default._syncQQ;
            MessageBox.Show("已经挂载到osu.\nBY ULIXES.\nNow we are in:" + Process.GetCurrentProcess().Id + " " + Process.GetCurrentProcess().MainWindowTitle, "direct!osu");
            
            if (!Core._hooked)
            {
                Application.Run(new mainForm(true));
            }
        }


    }
}
