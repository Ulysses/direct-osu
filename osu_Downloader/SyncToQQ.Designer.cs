﻿namespace osu_Downloader
{
    partial class SyncToQQ
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SyncToQQ));
            this.txt_QQ = new System.Windows.Forms.TextBox();
            this.btn_accept = new System.Windows.Forms.Button();
            this.btn_cancel = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.chk_diff = new System.Windows.Forms.CheckBox();
            this.chk_save = new System.Windows.Forms.CheckBox();
            this.chk_realName = new System.Windows.Forms.CheckBox();
            this.toolTip_qq = new System.Windows.Forms.ToolTip(this.components);
            this.SuspendLayout();
            // 
            // txt_QQ
            // 
            this.txt_QQ.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txt_QQ.Location = new System.Drawing.Point(12, 57);
            this.txt_QQ.Name = "txt_QQ";
            this.txt_QQ.Size = new System.Drawing.Size(165, 22);
            this.txt_QQ.TabIndex = 0;
            // 
            // btn_accept
            // 
            this.btn_accept.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_accept.Location = new System.Drawing.Point(369, 10);
            this.btn_accept.Name = "btn_accept";
            this.btn_accept.Size = new System.Drawing.Size(75, 35);
            this.btn_accept.TabIndex = 1;
            this.btn_accept.Text = "开始同步";
            this.btn_accept.UseVisualStyleBackColor = true;
            this.btn_accept.Click += new System.EventHandler(this.btn_accept_Click);
            // 
            // btn_cancel
            // 
            this.btn_cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_cancel.Location = new System.Drawing.Point(369, 53);
            this.btn_cancel.Name = "btn_cancel";
            this.btn_cancel.Size = new System.Drawing.Size(75, 32);
            this.btn_cancel.TabIndex = 2;
            this.btn_cancel.Text = "停止同步";
            this.btn_cancel.UseVisualStyleBackColor = true;
            this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 14);
            this.label1.TabIndex = 3;
            this.label1.Text = "请输入QQ号";
            // 
            // chk_diff
            // 
            this.chk_diff.AutoSize = true;
            this.chk_diff.Location = new System.Drawing.Point(193, 17);
            this.chk_diff.Name = "chk_diff";
            this.chk_diff.Size = new System.Drawing.Size(82, 18);
            this.chk_diff.TabIndex = 4;
            this.chk_diff.Text = "显示难度";
            this.toolTip_qq.SetToolTip(this.chk_diff, "勾选后会显示正在玩的难度（如Insane）\r\n即时保存，即时生效");
            this.chk_diff.UseVisualStyleBackColor = true;
            this.chk_diff.CheckedChanged += new System.EventHandler(this.chk_diff_CheckedChanged);
            // 
            // chk_save
            // 
            this.chk_save.AutoSize = true;
            this.chk_save.Location = new System.Drawing.Point(281, 17);
            this.chk_save.Name = "chk_save";
            this.chk_save.Size = new System.Drawing.Size(82, 18);
            this.chk_save.TabIndex = 5;
            this.chk_save.Text = "保存设置";
            this.toolTip_qq.SetToolTip(this.chk_save, "勾上后会(尝试)保存QQ号和是否同步。\r\n否则本次的设置只在direct!osu结束前有效。");
            this.chk_save.UseVisualStyleBackColor = true;
            // 
            // chk_realName
            // 
            this.chk_realName.AutoSize = true;
            this.chk_realName.Location = new System.Drawing.Point(193, 61);
            this.chk_realName.Name = "chk_realName";
            this.chk_realName.Size = new System.Drawing.Size(138, 18);
            this.chk_realName.TabIndex = 6;
            this.chk_realName.Text = "尝试显示真实曲名";
            this.toolTip_qq.SetToolTip(this.chk_realName, "勾上后会尝试显示Unicode曲名（如果有的话）\r\n这是一项实验性功能，会加大资源的消耗\r\n即时生效，即时保存");
            this.chk_realName.UseVisualStyleBackColor = true;
            this.chk_realName.CheckedChanged += new System.EventHandler(this.chk_realName_CheckedChanged);
            // 
            // SyncToQQ
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(456, 97);
            this.Controls.Add(this.chk_realName);
            this.Controls.Add(this.chk_save);
            this.Controls.Add(this.chk_diff);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btn_cancel);
            this.Controls.Add(this.btn_accept);
            this.Controls.Add(this.txt_QQ);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "SyncToQQ";
            this.Text = "direct!osu - QQ同步";
            this.Load += new System.EventHandler(this.SyncToQQ_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txt_QQ;
        private System.Windows.Forms.Button btn_accept;
        private System.Windows.Forms.Button btn_cancel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox chk_diff;
        private System.Windows.Forms.CheckBox chk_save;
        private System.Windows.Forms.ToolTip toolTip_qq;
        private System.Windows.Forms.CheckBox chk_realName;
    }
}