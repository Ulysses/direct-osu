﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;
using System.Media;

namespace osu_Downloader
{
    public static class Core
    {
        public static string _osupath;
        public static string _directPath;
        public static string _directStartupPath;
        public static string _QQ = "";
        public static string _nowPlaying = "";
        public static string _tempOsuUrl = "";
        [DllImport("user32.dll", SetLastError = true)]
        public static extern bool BringWindowToTop(IntPtr hWnd);
        [DllImport("user32.dll")]
        public static extern bool SetForegroundWindow(IntPtr hWnd);
        delegate void DlistUpdate(string progress, int key);
        static Regex _regx = new Regex(@"m\/[0-9]+\b");
        public static DownloadedMaps _downloadedmap = new DownloadedMaps();
        public static Queue<Core.Notify> _tipQ = new Queue<Core.Notify>();
        public static bool _isShowingTip = false;
        public static bool _syncQQ = false;
        public static bool _hooked = false;
        public static bool _disableHook = false;
        //public static Thread _tNotify;
        public static RichTextBox _txt;
        public static NotifyIcon _notifyIco;
        public static ListView _list_down;
        public static CheckBox _chk_autoReturn;
        public static ProgressBar _pbr_download;
        public static SoundPlayer player = new SoundPlayer();
        /// <summary>
        /// number,Thread
        /// </summary>
        public static Dictionary<string, Thread> _tDownload = new Dictionary<string, Thread>();

        public static void InitCore(RichTextBox txt, NotifyIcon notifyIco, CheckBox chk_autoReturn, ProgressBar pbr_download, ListView list)
        {
            _txt = txt;
            _notifyIco = notifyIco;
            _chk_autoReturn = chk_autoReturn;
            _pbr_download = pbr_download;
            _list_down = list;
        }
        public struct Notify
        {
            public int operate { get; set; }
            public string tip { get; set; }
            public Notify(int Operate, string Tip)
                : this()
            {
                operate = Operate;
                tip = Tip;
            }
        };

        public static void ShowNotify()
        {
            Core.Notify notify;
            string tipTitle = "提示";
            notify = Core._tipQ.Dequeue();
            if (notify.operate == 1)
            {
                tipTitle = "正在下载";
            }
            if (notify.operate == 2)
            {
                tipTitle = "已经存在该图";
            }
            if (notify.operate == 3)
            {
                tipTitle = "下载完毕";
            }
            //bool started = false;
            //if (_tNotify.ThreadState != System.Threading.ThreadState.WaitSleepJoin && _tNotify.ThreadState != System.Threading.ThreadState.Running)
            //{
            //    _tNotify = new Thread(NotifyTips);
            //    _tNotify.Start();
            //}
            if (_hooked == false)
            {
                _notifyIco.ShowBalloonTip(800, tipTitle, notify.tip, ToolTipIcon.Info);
            }
        }
        //public static void NotifyTips()
        //{
        //    Core.Notify notify;
        //    string tipTitle = "提示";
        //    try
        //    {
        //        while (Core._tipQ.Count > 0)
        //        {
        //            if (!_isShowingTip)
        //            {
        //                notify = Core._tipQ.Dequeue();
        //                if (notify.operate == 1)
        //                {
        //                    tipTitle = "正在下载";
        //                }
        //                if (notify.operate == 2)
        //                {
        //                    tipTitle = "已经存在该图";
        //                }
        //                if (notify.operate == 3)
        //                {
        //                    tipTitle = "下载完毕";
        //                }
        //                _notifyIco.ShowBalloonTip(800, tipTitle, notify.tip, ToolTipIcon.Info);
        //            }
        //            if (Core._tipQ.Count > 0)
        //            {
        //                Thread.Sleep(800);
        //            }
        //        }

        //    }
        //    catch (Exception)
        //    {
        //        //Console.WriteLine(ex);
        //    }
        //    finally
        //    {
        //        Core._tNotify.Abort();
        //    }
        //    return;
        //}
        /// <summary>
        /// 显示URL并准备下载
        /// </summary>
        /// <param name="url"></param>
        /// <param name="name"></param>
        public static bool ShowURL(string url, string name = "")
        {
            char mapType;
            //txt.Text += Environment.NewLine + _monitor.getTitle(_monitor.GetHTML(url)) + Environment.NewLine + url;
            Core._txt.AppendText(Environment.NewLine + name + Environment.NewLine + url);
            string number = GetNumber(url, out mapType);
            if (name == "")
            {
                name = "osu编号" + mapType + "/" + number;
            }
            if (_chk_autoReturn.Checked)
            {
                //SetWindowPos(Process.GetProcessesByName("osu!")[0].MainWindowHandle, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);
                SetForegroundWindow(Process.GetProcessesByName("osu!")[0].MainWindowHandle);
                BringWindowToTop(Process.GetProcessesByName("osu!")[0].MainWindowHandle);
                //SetActiveWindow(Process.GetProcessesByName("osu!")[0].MainWindowHandle);
                //SetFocus(Process.GetProcessesByName("osu!")[0].MainWindowHandle);
            }
            return GetBloodCatUrl(number, mapType, name);
        }
        public static string GetNumber(string url, out char mapType)
        {
            mapType = 'b';
            if (url.Contains("/s/"))
            {
                mapType = 's';
            }
            url = url.Remove(0, url.LastIndexOf('/') + 1);
            if (url.Contains('?'))
            {
                url = url.Remove(url.LastIndexOf('?'));
            }
            if (url.Contains('&'))
            {
                url = url.Remove(url.LastIndexOf('&'));
            }
            Console.WriteLine(url);
            return url;
        }
        public static bool GetBloodCatUrl(string number, char mapType, string name = "")
        {
            string htmlcode = Monitor.GetHTML("http://bloodcat.com/osu/?q=" + number + "&m=" + mapType);
            Match mat = _regx.Match(htmlcode);
            string realNumber = number;
            if (mat.Success)
            {
                if (mapType == 'b')
                {
                    realNumber = mat.Value.Remove(0, 2);
                }
                if (_downloadedmap.setIDs.Contains(realNumber))
                {
                    Core._tipQ.Enqueue(new Core.Notify(2, "Set ID = " + realNumber));
                    Core.ShowNotify();
                    player.Stream = Properties.Resources.soft_hitnormal2;
                    player.Play();
                    Core._txt.AppendText(Environment.NewLine + "检测到已有此图，已停止下载。" + Environment.NewLine + "如需强制下载可用此链接： " + "http://bloodcat.com/osu/m/" + realNumber);

                    return false;
                }
                {
                    Core._tipQ.Enqueue(new Core.Notify(1, name));
                    Core.ShowNotify();
                }
                if (_tDownload.Keys.Contains(realNumber))
                {
                    player.Stream = Properties.Resources.soft_hitnormal2;
                    player.Play();
                    Core._txt.AppendText(Environment.NewLine + "无法启动下载，可能该图正在下载中？");
                    return false;
                }
                else
                {
                    Thread th = new Thread(Core.sync);
                    if (Core._tDownload.Keys.Contains(realNumber))
                    {
                        Core._tDownload.Remove(realNumber);
                    }
                    _tDownload.Add(realNumber, th);
                    if (_list_down.FindItemWithText(number) == null)
                    {
                        _list_down.Items.Add(new ListViewItem(new string[] { mat.Value, "0%", "", mat.Value.Remove(0, 2), "" })).Selected = true;
                    }
                    th.Start(mat.Value);
                }
                _downloadedmap.setIDs.Add(realNumber);
                if (mapType == 'b')
                {
                    _downloadedmap.beatmapIDs.Add(number);
                }
            }
            else
            {
                player.Stream = Properties.Resources.soft_hitnormal2;
                player.Play();
                Core._txt.AppendText(Environment.NewLine + "没有找到这张图T^T...");
                return false;
            }
            return true;
        }
        public static void pbrUpdate(object sender, EventArgs e)
        {
            int p;
            if (_list_down.SelectedItems.Count == 0)
            {
                _list_down.Items[0].Selected = true;
            }
            if (int.TryParse(_list_down.SelectedItems[0].SubItems[1].Text.Replace("%", ""), out p))
            {
                p = Math.Abs(p);
                if (p <= 100 && p >= 0)
                {
                    _pbr_download.Value = p;
                    if (p % 10 == 0)
                    {
                        player.Stream = Properties.Resources.normal_slidertick;
                        player.Play();
                    }
                }
            }
            //_pbr_download.Value = int.Parse();
            //if ((int)sender == 100)
            //btn_download.Enabled = true;
        }

        /// <summary>
        /// Ray下载方式
        /// </summary>
        /// <param name="obj"></param>
        public static void sync(object obj)
        {
            if (Core._directStartupPath == null || Core._directStartupPath == "")
            {
                Core._directStartupPath = Properties.Settings.Default._savedPath;
            }
            player.Stream = Properties.Resources.soft_hitfinish;
            player.Play();
            string index = (string)obj;
            string number = index.Remove(0, 2);
            int key = 0;
            ListViewItem lItem = new ListViewItem(new string[] { index, "0%", "", number, "" });

            if (!Directory.Exists(_directStartupPath + "\\Download"))
            {
                Directory.CreateDirectory(_directStartupPath + "\\Download");
            }
            BeatmapDownloader bDownload = new BeatmapDownloader(number, _directStartupPath + "\\Download\\");
            try
            {
                _list_down.BeginInvoke(new MethodInvoker(
                       delegate
                       {
                           if (_list_down.Items.Count == 0)
                           {
                               _list_down.Items.Add(lItem);
                           }
                           lItem = _list_down.FindItemWithText(index);
                           key = lItem.Index;
                           lItem.SubItems[2].Text = (bDownload.GetFileLength() / 1048576).ToString(".0") + "M";
                           lItem.SubItems[3].Text = number;
                           lItem.SubItems[4].Text = bDownload.GetFileName().Replace(number, "").Replace(".osz", "").Trim();
                       }));
                bDownload.ProgressChanged += (progress, setid) =>
                    {

                        _list_down.Invoke(new DlistUpdate(listUpdate), progress.ToString() + "%", key);
                        _pbr_download.Invoke(new EventHandler(pbrUpdate));
                    };
                bDownload.DownloadFinished += (setid) =>
                    {
                        _tipQ.Enqueue(new Notify(3, setid));
                        ShowNotify();
                        _list_down.BeginInvoke(new MethodInvoker(delegate { lItem.SubItems[1].Text = "100%"; }));
                        _pbr_download.Invoke(new EventHandler(pbrUpdate));
                        player.Stream = Properties.Resources.normal_hitfinish;
                        player.Play();
                        Thread.Sleep(1000);
                        //MessageBox.Show(dl.SavePath + "\n" + Application.StartupPath, Core._directStartupPath);
                        Process.Start(_directStartupPath + "\\Download\\" + bDownload.GetFileName());
                        _downloadedmap.setIDs.Add(number);
                    };
                bDownload.DownloadBeatMap();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                bDownload.Stop();
            }
            finally
            {
                if (!bDownload.IsCompleted)
                {
                    player.Stream = Properties.Resources.combobreak;
                    player.Play();
                    _list_down.BeginInvoke(new MethodInvoker(delegate { lItem.SubItems[1].Text = "下载失败"; }));
                }
                bDownload.Dispose();
            }
        }

        /// <summary>
        /// 多线程下载方式
        /// </summary>
        /// <param name="obj"></param>
        public static void Multisync(object obj)
        {
            player.Stream = Properties.Resources.soft_hitfinish;
            player.Play();
            string index = (string)obj;
            string number = index.Remove(0, 2);
            ListViewItem lItem = new ListViewItem(new string[] { index, "0%", "", number, "" });

            if (!Directory.Exists(_directStartupPath + "\\Download"))
            {
                Directory.CreateDirectory(_directStartupPath + "\\Download");
            }
            Downloader dl = new Downloader(1, "http://bloodcat.com/osu/" + index, _directStartupPath + "\\Download");
            try
            {
                string progress = "0%";
                string oldProgress;
                int key = 0;

                dl.ExtName = "";
                dl.Start();
                _list_down.BeginInvoke(new MethodInvoker(
                    delegate
                    {
                        if (_list_down.Items.Count == 0)
                        {
                            _list_down.Items.Add(lItem);
                        }
                        lItem = _list_down.FindItemWithText(index);
                        key = lItem.Index;
                        lItem.SubItems[2].Text = (dl.FileSize / 1048576).ToString(".0") + "M";
                        lItem.SubItems[3].Text = number;
                        lItem.SubItems[4].Text = dl.FileName.Replace(number, "").Replace(".osz", "").Trim();
                    }));
                while (!dl.IsComplete)
                {
                    oldProgress = progress;
                    progress = (dl.DownloadSize * 100 / dl.FileSize).ToString() + "%";
                    if (progress != oldProgress)
                    {
                        _list_down.Invoke(new DlistUpdate(listUpdate), progress, key);
                        _pbr_download.Invoke(new EventHandler(pbrUpdate));
                    }
                }
                _tipQ.Enqueue(new Notify(3, number));
                ShowNotify();
                _list_down.BeginInvoke(new MethodInvoker(delegate { lItem.SubItems[1].Text = "100%"; }));
                _pbr_download.Invoke(new EventHandler(pbrUpdate));
                player.Stream = Properties.Resources.normal_hitfinish;
                player.Play();
                Thread.Sleep(1000);
                Process.Start(dl.SavePath + "\\" + dl.FileName);
                _downloadedmap.setIDs.Add(number);
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString());
                Console.WriteLine(ex);
                //throw;
            }
            finally
            {
                if (!dl.IsComplete)
                {
                    player.Stream = Properties.Resources.combobreak;
                    player.Play();
                    _list_down.BeginInvoke(new MethodInvoker(delegate { lItem.SubItems[1].Text = "下载失败"; }));
                }
                dl.Stop();
                dl.Dispose();
            }

        }
        private static void listUpdate(string progress, int key)
        {
            _list_down.Items[key].SubItems[1].Text = progress;
        }

        /// <summary>
        /// 原始下载方式
        /// </summary>
        /// <param name="obj"></param>
        public static void SingleDownload(object obj)
        {
            try
            {
                string index = (string)obj;
                HttpWebRequest request = WebRequest.Create("http://bloodcat.com/osu/" + index) as HttpWebRequest;
                request.Method = "GET";
                request.UserAgent = "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; SV1; .NET CLR 1.1.4322; .NET CLR 2.0.50727)";
                HttpWebResponse response = request.GetResponse() as HttpWebResponse;

                string headers = response.Headers["Content-Disposition"];
                long fileLength = response.ContentLength;
                int headst = headers.IndexOf("filename=") + 1;
                int headend = headers.IndexOf(".osz", headst);
                string filename = headers.Substring(headst + 9, headend - headst - 5);
                //pbr_download.BeginInvoke(new System.EventHandler(Update), filename);
                FileStream fs_download = new FileStream(filename, FileMode.Create);
                Stream myStream = response.GetResponseStream();
                byte[] btContent = new byte[512];
                int intSize = 0;
                long downloaded = 0;
                intSize = myStream.Read(btContent, 0, 512);
                while (intSize > 0)
                {
                    downloaded += intSize;
                    _pbr_download.BeginInvoke(new System.EventHandler(pbrUpdate), Convert.ToInt32(downloaded * 100 / fileLength));
                    fs_download.Write(btContent, 0, intSize);
                    intSize = myStream.Read(btContent, 0, 512);
                }
                fs_download.Close();
                myStream.Close();
                //txt.BeginInvoke(new System.EventHandler(txtUpdate), filename);
                System.Diagnostics.Process.Start(filename);
            }
            catch (System.ArgumentNullException)
            {
                MessageBox.Show("Cannot find the beatmap from bloodcat.com!", "Error");
            }
            finally
            {
                Application.ExitThread();
            }
        }
    }
}
