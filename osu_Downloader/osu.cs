﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace osu_Downloader
{
    public class osu
    {
        static string[] osuDefault = { "D:\\Program Files (x86)\\osu!", "C:\\Program Files (x86)\\osu!", "C:\\Program Files\\osu!", "D:\\Program Files\\osu!" ,Properties.Settings.Default._savedPath};
        public static bool FindOsuDefault(ref string osupath, Label lbl_path, RichTextBox txt, ref List<string> downloadedmap, ref List<string> downloadmapname)
        {
            bool getosu = false;
            foreach (string path in osuDefault)
            {
                if (File.Exists(path + "\\osu!.exe"))
                {
                    osupath = path;
                    downloadedmap.AddRange(osu.GetExistMaps(osupath + "\\Songs", ref downloadmapname));
                    getosu = true;
                }
                break;
            }            
            if (getosu == true)
            {
                lbl_path.Text = "已在默认目录找到osu:" + osupath;
                Properties.Settings.Default._savedPath = osupath;
                Core._txt.AppendText(Environment.NewLine + "已扫描到" + downloadedmap.Count() + "个曲目");
            }
            return getosu;
        }

        public static bool Setpath(ref string osupath)
        {
            FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();
            folderBrowserDialog.ShowNewFolderButton = false;
            folderBrowserDialog.RootFolder = Environment.SpecialFolder.MyComputer;
            folderBrowserDialog.Description = "请选择与osu!.exe同级的目录~";
            bool result;
            try
            {
                if (DialogResult.OK == folderBrowserDialog.ShowDialog())
                {
                    if (File.Exists(folderBrowserDialog.SelectedPath + "\\osu!.exe"))
                    {
                        osupath = folderBrowserDialog.SelectedPath;
                        result = true;
                        return result;
                    }
                    MessageBox.Show("为什么你总是想卖萌OAO", "direct!miss", MessageBoxButtons.OK);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("为什么你总是想卖萌OxO", "direct!exception", MessageBoxButtons.OK);
            }
            result = false;
            return result;
        }
        public static List<string> GetExistMaps(string osupath, ref List<string> names)
        {
            List<string> tempMap = new List<string>();
            Regex _regx = new Regex(@"^[0-9]+\b");
            Match mat;
            string[] dirs = Directory.GetDirectories(osupath, "*", SearchOption.TopDirectoryOnly);
            foreach (string dir in dirs)
            {
                DownloadedMaps.dirs.Add(dir);
                string number = dir.Remove(0, dir.LastIndexOf('\\') + 1);
                mat = _regx.Match(number);
                if (mat.Success)
                {
                    tempMap.Add(mat.Value);
                    names.Add(number.Replace(mat.Value, "").Trim());
                }
            }
            return tempMap;
        }
        public static string GetSongName(string engName, out string artistName,bool remainDiff = false)
        {
            artistName = "";
            string diff = "[Insane]";
            string songName ="";
            string engSongName;
            string engArtistName = engName.Remove(engName.LastIndexOf(" - ")).Trim();
            if (engName.Contains('['))
            {
                diff = engName.Remove(0, engName.LastIndexOf('['));
                engSongName = engName.Remove(engName.LastIndexOf('[')).Trim();
            }
            else
            {
                engSongName = engName.Trim();
            }
            var findedMaps = DownloadedMaps.dirs.Where(dir => dir.Contains(engSongName));
            foreach (string map in findedMaps)
            {
                string osufile;
                string[] bfiles = Directory.GetFiles(map, "*.osu");
                if (bfiles.Count() >0)
                {
                    osufile = File.ReadAllText(bfiles[0]);
                    if (osufile.Contains("TitleUnicode:"))
                    {
                        songName = osufile.Remove(0, osufile.IndexOf("TitleUnicode:")+13);
                        songName = songName.Remove(songName.IndexOf("\n"));
                        songName = songName.Replace("\r", "").Trim();
                        artistName = osufile.Remove(0, osufile.IndexOf("ArtistUnicode:")+14);
                        artistName = artistName.Remove(artistName.IndexOf("\n"));
                        artistName = artistName.Replace("\r", "").Trim();
                    }
                }
            }
            if (artistName == null || artistName=="")
            {
                artistName = engArtistName;
            }
            if (songName == null || songName == "")
            {
                songName = engSongName.Remove(0, engSongName.LastIndexOf(" - ")+3);
            }
            if (remainDiff)
            {
                songName += " " + diff;
            }
            return songName;
        }

    }
}
