﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;

namespace osu_Downloader
{
    public delegate void ProgressChangeEventHandler(int progress, string setId);

    public delegate void DownloadFinishedEventHandler(string setId);

    internal class BeatmapDownloader : IDisposable
    {
        private const string DownloadUrl = "http://bloodcat.com/osu/m/";
        private long _downloaded;
        private long _fileLength;
        private string _fileName;
        private string _savePath;
        private int _progress;
        private HttpWebResponse _response;
        public string setId;
        public bool IsCompleted = false;
        private bool _allowDownload = true;
        FileStream fsDownload;
        Stream myStream;

        public BeatmapDownloader(string setId)
        {
            this.setId = setId;
            SetResponse(setId);
        }

        public BeatmapDownloader(string setId,string savePath)
        {
            this.setId = setId;
            _savePath = savePath;
            SetResponse(setId);
        }

        public event ProgressChangeEventHandler ProgressChanged;
        public event DownloadFinishedEventHandler DownloadFinished;

        public string GetSetId(string beatmapId)
        {
            return beatmapId;
        }

        public string GetFileName()
        {
            string headers = _response.Headers["Content-Disposition"];
            int headst = headers.IndexOf("filename=", StringComparison.Ordinal);
            int headend = headers.IndexOf(".osz", headst, StringComparison.Ordinal);
            _fileName = headers.Substring(headst + 10, headend - headst - 6);
            return _fileName;
        }

        private void SetResponse(string setId)
        {
            _response = HttpWebResponseUtility.CreateGetHttpResponse(DownloadUrl + setId, null, null, null);
        }

        public long GetFileLength()
        {
            return _response.ContentLength;
        }

        public int GetProgress()
        {
            int progress = Convert.ToInt32((_downloaded*100L)/_fileLength);
            return progress;
        }
        public void Stop()
        {
            _allowDownload = false;
        }

        public bool CanDownload()
        {
            if (_response.Headers["Content-Disposition"] != null)
            {
                return true;
            }
            return false;
        }

        public string DownloadBeatMap()
        {
            GetFileName();
            myStream = _response.GetResponseStream();
            fsDownload = new FileStream(_savePath + _fileName, FileMode.Create);
            _fileLength = GetFileLength();

            var btContent = new byte[512];
            _downloaded = 0;
            _progress = -1;

            if (myStream != null)
            {
                int intSize = myStream.Read(btContent, 0, 512);
                while (intSize > 0 && _allowDownload)
                {
                    _downloaded += intSize;
                    ;
                    if (_progress != GetProgress() && ProgressChanged != null)
                    {
                        _progress = GetProgress();
                        ProgressChanged(_progress, setId);
                    }
                    fsDownload.Write(btContent, 0, intSize);
                    intSize = myStream.Read(btContent, 0, 512);
                }
                fsDownload.Close();
                myStream.Close();
            }
            if (DownloadFinished != null && _allowDownload)
            {
                IsCompleted = true;
                DownloadFinished(setId); 
            }
            return _fileName;
        }

        public void Dispose()
        {
            if (fsDownload != null)
            {
                fsDownload.Close();
                fsDownload.Dispose();
            }
            if (myStream != null)
            {
                myStream.Close();
                myStream.Dispose();
            }
            GC.Collect();
        }
    }
}