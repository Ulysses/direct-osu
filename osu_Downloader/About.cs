﻿using System;
using System.Windows.Forms;
using System.Diagnostics;

namespace osu_Downloader
{
    public partial class About : Form
    {
        public About()
        {
            InitializeComponent();
        }

        private void About_Load(object sender, EventArgs e)
        {
        }

        private void link2_Click(object sender, EventArgs e)
        {
            Process.Start("mailto:"+((LinkLabel)sender).Text);
        }
    }
}
