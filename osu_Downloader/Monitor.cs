﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Text.RegularExpressions;

namespace osu_Downloader
{
    class Monitor
    {
        static List<string> sbStorage = new List<string>();
        string keyword;
        string url;
        ///<summary>
        ///构造函数
        ///</summary>
        public Monitor()
        {

        }
        public Monitor(string keyword, string url)
        {
            this.keyword = keyword;
            this.url = url;
        }


        ///<summary>
        ///监听网站URL
        ///</summary>
        public static Dictionary<string,string> monitorUrl()
        {
            SHDocVw.ShellWindows sws = new SHDocVw.ShellWindows();
            Dictionary<string, string> sb = new Dictionary<string, string>();
            foreach (SHDocVw.WebBrowser iw in sws)
            {                
                if (iw.ToString() != "" && iw.ToString().Length > 7)
                {
                    if ((iw.LocationURL.ToString().Contains("http://")))
                    {
                        sb.Add(iw.LocationURL.ToString(), iw.LocationName.ToString());
                    }
                }
            }
            return sb;
        }
        public static List<string> monitorUrlinList()
        {
            SHDocVw.ShellWindows sws = new SHDocVw.ShellWindows();
            List<string> sb = new List<string>();
            foreach (SHDocVw.WebBrowser iw in sws)
            {
                if (iw.ToString() != "" && iw.ToString().Length > 7)
                {
                    if ((iw.LocationURL.ToString().Contains("http://")))
                    {
                        sb.Add(iw.LocationURL.ToString());
                    }
                }
            }
            return sb;
        }

        ///<summary>
        ///获取HTML代码
        ///</summary>
        public static string GetHTML(string url)
        {                     
            WebClient myWebClient = new WebClient(); //创建WebClient实例myWebClient 
            // 需要注意的： 
            //有的网页可能下不下来，有种种原因比如需要cookie,编码问题等等 
            //这是就要具体问题具体分析比如在头部加入cookie 
            // webclient.Headers.Add("Cookie", cookie); 
            //这样可能需要一些重载方法。根据需要写就可以了 

            //获取或设置用于对向 Internet 资源的请求进行身份验证的网络凭据。 
            myWebClient.Credentials = CredentialCache.DefaultCredentials;
            //如果服务器要验证用户名,密码 
            //NetworkCredential mycred = new NetworkCredential(struser, strpassword); 
            //myWebClient.Credentials = mycred; 
            //从资源下载数据并返回字节数组。（加@是因为网址中间有"/"符号） 
            byte[] myDataBuffer = myWebClient.DownloadData(url);
            string strWebData = Encoding.Default.GetString(myDataBuffer);

            //获取网页字符编码描述信息 
            Match charSetMatch = Regex.Match(strWebData, "<meta([^<]*)charset=([^<]*)\"", RegexOptions.IgnoreCase | RegexOptions.Multiline);
            string webCharSet = charSetMatch.Groups[2].Value;

            if (Encoding.GetEncoding(webCharSet).ToString() != "")
            {
                strWebData = Encoding.GetEncoding(webCharSet).GetString(myDataBuffer);
            }
            else
            {
                strWebData = Encoding.Default.GetString(myDataBuffer);
            }
            return strWebData;



        }

        /// <summary>
        /// 检索HTML是否存在关键字
        /// </summary>
        /// <param name="kyd"></param>
        /// <param name="htmlcode"></param>
        /// <returns></returns>
        public bool retrivalKyw(string kyd, string htmlcode)
        {
            //查找HTML代码中是否有指定的关键字,并放回结果。
            return htmlcode.ToString().Contains(kyd);
        }

        /// <summary>
        /// 获取HTML标题（title）
        /// </summary>
        /// <param name="htmlcode"></param>
        /// <returns></returns>
        public string getTitle(string htmlcode)
        {
            string title = null;
            string titile1 = "</title>";
            if ((htmlcode.Contains("<title>") == true) && (htmlcode.Contains(titile1) == true))
            {
                title = htmlcode.Substring((htmlcode.IndexOf("<title>") + 7), (htmlcode.IndexOf("</title>")));
                if (title.IndexOf("<") != -1)
                {
                    title = title.Substring(0, title.IndexOf("<"));
                }
            }
            else
            {
                title = "标题不存在或提取时出错";
            }
            return title;
        }


        /// <summary>
        /// 判断网页是否监听处理过，如果处理过就不在处理
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static bool judgeUrl(string url)
        {

            if (sbStorage.Contains(url))
            {
                return false;
            }
            sbStorage.Add(url);
            return true;
        }
    }
}
