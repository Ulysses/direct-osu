﻿namespace osu_Downloader
{
    partial class mainForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(mainForm));
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.notifyIco = new System.Windows.Forms.NotifyIcon(this.components);
            this.pbr_download = new System.Windows.Forms.ProgressBar();
            this.btn_Chooseosu = new System.Windows.Forms.Button();
            this.lbl_path = new System.Windows.Forms.Label();
            this.rdo_disable = new System.Windows.Forms.RadioButton();
            this.rdo_manual = new System.Windows.Forms.RadioButton();
            this.rdo_auto = new System.Windows.Forms.RadioButton();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.chk_autoReturn = new System.Windows.Forms.CheckBox();
            this.btn_hook = new System.Windows.Forms.Button();
            this.btn_unhook = new System.Windows.Forms.Button();
            this.panel_setHook = new System.Windows.Forms.Panel();
            this.rdo_ALLHook = new System.Windows.Forms.RadioButton();
            this.rdo_SEHook = new System.Windows.Forms.RadioButton();
            this.rdo_CPHook = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_syncQQ = new System.Windows.Forms.Button();
            this.pic_direct = new System.Windows.Forms.PictureBox();
            this.btn_about = new System.Windows.Forms.Button();
            this.lbl_hooked = new System.Windows.Forms.Label();
            this.list_down = new System.Windows.Forms.ListView();
            this.dURL = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.dProgress = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.dSize = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.dSetID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.dName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.menu_list = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mStop = new System.Windows.Forms.ToolStripMenuItem();
            this.mRestart = new System.Windows.Forms.ToolStripMenuItem();
            this.mInfo = new System.Windows.Forms.ToolStripMenuItem();
            this.mCopy = new System.Windows.Forms.ToolStripMenuItem();
            this.mOgl = new System.Windows.Forms.ToolStripMenuItem();
            this.mYas = new System.Windows.Forms.ToolStripMenuItem();
            this.txt = new System.Windows.Forms.RichTextBox();
            this.panel_setHook.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pic_direct)).BeginInit();
            this.menu_list.SuspendLayout();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.WorkerReportsProgress = true;
            this.backgroundWorker1.WorkerSupportsCancellation = true;
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            this.backgroundWorker1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker1_RunWorkerCompleted);
            // 
            // notifyIco
            // 
            this.notifyIco.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIco.Icon")));
            this.notifyIco.Text = "Direct!osu";
            this.notifyIco.Visible = true;
            this.notifyIco.BalloonTipClosed += new System.EventHandler(this.notifyIcon1_BalloonTipClosed);
            this.notifyIco.BalloonTipShown += new System.EventHandler(this.notifyIcon1_BalloonTipShown);
            // 
            // pbr_download
            // 
            this.pbr_download.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pbr_download.Location = new System.Drawing.Point(12, 487);
            this.pbr_download.Name = "pbr_download";
            this.pbr_download.Size = new System.Drawing.Size(478, 23);
            this.pbr_download.TabIndex = 1;
            // 
            // btn_Chooseosu
            // 
            this.btn_Chooseosu.Location = new System.Drawing.Point(174, 4);
            this.btn_Chooseosu.Name = "btn_Chooseosu";
            this.btn_Chooseosu.Size = new System.Drawing.Size(129, 23);
            this.btn_Chooseosu.TabIndex = 2;
            this.btn_Chooseosu.Text = "请先选择osu目录";
            this.toolTip1.SetToolTip(this.btn_Chooseosu, "如果没有能在默认目录侦测到osu，\r\n可以在这里选择osu所在目录，\r\n以提供重复图检测功能\r\n（不用选也可以）\r\n（Hook模式成功后会尝试探测目录）");
            this.btn_Chooseosu.UseVisualStyleBackColor = true;
            this.btn_Chooseosu.Click += new System.EventHandler(this.btn_Chooseosu_Click);
            // 
            // lbl_path
            // 
            this.lbl_path.AutoSize = true;
            this.lbl_path.Location = new System.Drawing.Point(12, 71);
            this.lbl_path.Name = "lbl_path";
            this.lbl_path.Size = new System.Drawing.Size(0, 14);
            this.lbl_path.TabIndex = 3;
            this.lbl_path.TextChanged += new System.EventHandler(this.lbl_path_TextChanged);
            this.lbl_path.Click += new System.EventHandler(this.lbl_path_Click);
            // 
            // rdo_disable
            // 
            this.rdo_disable.AutoSize = true;
            this.rdo_disable.Checked = true;
            this.rdo_disable.Location = new System.Drawing.Point(81, 96);
            this.rdo_disable.Name = "rdo_disable";
            this.rdo_disable.Size = new System.Drawing.Size(67, 18);
            this.rdo_disable.TabIndex = 4;
            this.rdo_disable.TabStop = true;
            this.rdo_disable.Text = "不启用";
            this.toolTip1.SetToolTip(this.rdo_disable, "不启用普通模式。");
            this.rdo_disable.UseVisualStyleBackColor = true;
            this.rdo_disable.CheckedChanged += new System.EventHandler(this.rdo_auto_CheckedChanged);
            // 
            // rdo_manual
            // 
            this.rdo_manual.AutoSize = true;
            this.rdo_manual.Location = new System.Drawing.Point(150, 96);
            this.rdo_manual.Name = "rdo_manual";
            this.rdo_manual.Size = new System.Drawing.Size(67, 18);
            this.rdo_manual.TabIndex = 5;
            this.rdo_manual.Text = "半自动";
            this.toolTip1.SetToolTip(this.rdo_manual, "半自动模式\r\n需要您把铺面网址复制到剪切板中（选中整个网址，Ctrl+C），检测到后自动下载。\r\n请注意该模式不检测重复！");
            this.rdo_manual.UseVisualStyleBackColor = true;
            this.rdo_manual.AutoSizeChanged += new System.EventHandler(this.rdo_auto_CheckedChanged);
            // 
            // rdo_auto
            // 
            this.rdo_auto.AutoSize = true;
            this.rdo_auto.Location = new System.Drawing.Point(223, 96);
            this.rdo_auto.Name = "rdo_auto";
            this.rdo_auto.Size = new System.Drawing.Size(67, 18);
            this.rdo_auto.TabIndex = 6;
            this.rdo_auto.Text = "全自动";
            this.toolTip1.SetToolTip(this.rdo_auto, "全自动模式\r\n自动监测浏览器内的网址,如有曲包页面自动开始下载.\r\n有一定的检测重复能力,但并不十分可靠.\r\n对IE支持最佳,其它浏览器可能需等到页面加载完毕才能" +
        "开始.\r\n支持Chrome,Firefox,Safari(理论).\r\n如不能使用本模式请切换至半自动.\r\n在本模式下请勿大量打开浏览器页面!");
            this.rdo_auto.UseVisualStyleBackColor = true;
            this.rdo_auto.CheckedChanged += new System.EventHandler(this.rdo_auto_CheckedChanged);
            // 
            // toolTip1
            // 
            this.toolTip1.AutoPopDelay = 12000;
            this.toolTip1.InitialDelay = 500;
            this.toolTip1.IsBalloon = true;
            this.toolTip1.ReshowDelay = 100;
            // 
            // chk_autoReturn
            // 
            this.chk_autoReturn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chk_autoReturn.AutoSize = true;
            this.chk_autoReturn.Location = new System.Drawing.Point(384, 97);
            this.chk_autoReturn.Name = "chk_autoReturn";
            this.chk_autoReturn.Size = new System.Drawing.Size(103, 18);
            this.chk_autoReturn.TabIndex = 7;
            this.chk_autoReturn.Text = "自动返回osu";
            this.toolTip1.SetToolTip(this.chk_autoReturn, "勾选后，在自动和半自动模式下开始地图下载后会返回osu游戏界面。\r\n必须事先开启osu才能使用，否则不能勾选。勾选后可能出错。\r\n对半自动模式的BloodCat链" +
        "接无效。");
            this.chk_autoReturn.UseVisualStyleBackColor = true;
            this.chk_autoReturn.CheckedChanged += new System.EventHandler(this.chk_autoReturn_CheckedChanged);
            // 
            // btn_hook
            // 
            this.btn_hook.ForeColor = System.Drawing.SystemColors.Highlight;
            this.btn_hook.Location = new System.Drawing.Point(12, 4);
            this.btn_hook.Name = "btn_hook";
            this.btn_hook.Size = new System.Drawing.Size(75, 23);
            this.btn_hook.TabIndex = 10;
            this.btn_hook.Text = "Hook模式";
            this.toolTip1.SetToolTip(this.btn_hook, "Hook模式 [请通过此程序打开osu]\r\n能高效地获取地址并开始下载。\r\n如果有效的话建议启用该模式。\r\n但是由于杀毒软件、兼容性及其他因素，\r\n可能会无效。不" +
        "支持IE6！\r\n启用该模式后请不要再使用普通模式！");
            this.btn_hook.UseVisualStyleBackColor = true;
            this.btn_hook.Click += new System.EventHandler(this.btn_hook_Click);
            // 
            // btn_unhook
            // 
            this.btn_unhook.ForeColor = System.Drawing.SystemColors.Highlight;
            this.btn_unhook.Location = new System.Drawing.Point(93, 4);
            this.btn_unhook.Name = "btn_unhook";
            this.btn_unhook.Size = new System.Drawing.Size(75, 23);
            this.btn_unhook.TabIndex = 12;
            this.btn_unhook.Text = "解除Hook";
            this.toolTip1.SetToolTip(this.btn_unhook, "手动解除Hook模式\r\n（正常关闭程序时会自动解除）\r\n");
            this.btn_unhook.UseVisualStyleBackColor = true;
            this.btn_unhook.Click += new System.EventHandler(this.btn_unhook_Click);
            // 
            // panel_setHook
            // 
            this.panel_setHook.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel_setHook.Controls.Add(this.rdo_ALLHook);
            this.panel_setHook.Controls.Add(this.rdo_SEHook);
            this.panel_setHook.Controls.Add(this.rdo_CPHook);
            this.panel_setHook.Location = new System.Drawing.Point(174, 33);
            this.panel_setHook.Name = "panel_setHook";
            this.panel_setHook.Size = new System.Drawing.Size(316, 22);
            this.panel_setHook.TabIndex = 14;
            this.toolTip1.SetToolTip(this.panel_setHook, "从这里选择Hook模式中使用的Hook\r\n如果当前设置无效可以换用另一个\r\n请在Hook模式开启后再设置");
            // 
            // rdo_ALLHook
            // 
            this.rdo_ALLHook.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.rdo_ALLHook.AutoSize = true;
            this.rdo_ALLHook.Location = new System.Drawing.Point(86, 1);
            this.rdo_ALLHook.Name = "rdo_ALLHook";
            this.rdo_ALLHook.Size = new System.Drawing.Size(53, 18);
            this.rdo_ALLHook.TabIndex = 16;
            this.rdo_ALLHook.Text = "默认";
            this.toolTip1.SetToolTip(this.rdo_ALLHook, "同时启用后面两种模式\r\n确保最大兼容性，但也可能出错\r\n如果你更喜欢某一种，请单独选择之");
            this.rdo_ALLHook.UseVisualStyleBackColor = true;
            this.rdo_ALLHook.CheckedChanged += new System.EventHandler(this.rdo_SEHook_CheckedChanged);
            // 
            // rdo_SEHook
            // 
            this.rdo_SEHook.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.rdo_SEHook.AutoSize = true;
            this.rdo_SEHook.Checked = true;
            this.rdo_SEHook.Location = new System.Drawing.Point(232, 1);
            this.rdo_SEHook.Name = "rdo_SEHook";
            this.rdo_SEHook.Size = new System.Drawing.Size(81, 18);
            this.rdo_SEHook.TabIndex = 15;
            this.rdo_SEHook.TabStop = true;
            this.rdo_SEHook.Text = "兼容Hook";
            this.toolTip1.SetToolTip(this.rdo_SEHook, "监测ShellExecute\r\n在Win8和WinXP上可能有更好的兼容性\r\n但也可能会卡");
            this.rdo_SEHook.UseVisualStyleBackColor = true;
            this.rdo_SEHook.CheckedChanged += new System.EventHandler(this.rdo_SEHook_CheckedChanged);
            // 
            // rdo_CPHook
            // 
            this.rdo_CPHook.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.rdo_CPHook.AutoSize = true;
            this.rdo_CPHook.Location = new System.Drawing.Point(145, 1);
            this.rdo_CPHook.Name = "rdo_CPHook";
            this.rdo_CPHook.Size = new System.Drawing.Size(81, 18);
            this.rdo_CPHook.TabIndex = 0;
            this.rdo_CPHook.Text = "普通Hook";
            this.toolTip1.SetToolTip(this.rdo_CPHook, "监测CreateProcess\r\n效率可能更高一些，不容易出错");
            this.rdo_CPHook.UseVisualStyleBackColor = true;
            this.rdo_CPHook.CheckedChanged += new System.EventHandler(this.rdo_SEHook_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 99);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 14);
            this.label1.TabIndex = 16;
            this.label1.Text = "普通模式";
            this.toolTip1.SetToolTip(this.label1, "普通模式\r\n通过剪切板\r\n或是监测浏览器活动\r\n获取osu谱面的下载链接。");
            // 
            // btn_syncQQ
            // 
            this.btn_syncQQ.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_syncQQ.Location = new System.Drawing.Point(390, 4);
            this.btn_syncQQ.Name = "btn_syncQQ";
            this.btn_syncQQ.Size = new System.Drawing.Size(100, 23);
            this.btn_syncQQ.TabIndex = 18;
            this.btn_syncQQ.Text = "osu同步至QQ";
            this.toolTip1.SetToolTip(this.btn_syncQQ, "将osu正在进行的歌曲实时同步到已登录的QQ的音乐状态");
            this.btn_syncQQ.UseVisualStyleBackColor = true;
            this.btn_syncQQ.Click += new System.EventHandler(this.btn_syncQQ_Click);
            // 
            // pic_direct
            // 
            this.pic_direct.BackColor = System.Drawing.Color.Transparent;
            this.pic_direct.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pic_direct.Dock = System.Windows.Forms.DockStyle.Right;
            this.pic_direct.Image = global::osu_Downloader.Properties.Resources.menu_directosu_over;
            this.pic_direct.Location = new System.Drawing.Point(466, 0);
            this.pic_direct.Name = "pic_direct";
            this.pic_direct.Size = new System.Drawing.Size(100, 522);
            this.pic_direct.TabIndex = 8;
            this.pic_direct.TabStop = false;
            this.toolTip1.SetToolTip(this.pic_direct, "双击启动osu!");
            this.pic_direct.DoubleClick += new System.EventHandler(this.pic_direct_DoubleClick);
            // 
            // btn_about
            // 
            this.btn_about.Location = new System.Drawing.Point(309, 4);
            this.btn_about.Name = "btn_about";
            this.btn_about.Size = new System.Drawing.Size(75, 23);
            this.btn_about.TabIndex = 9;
            this.btn_about.Text = "关于";
            this.btn_about.UseVisualStyleBackColor = true;
            this.btn_about.Click += new System.EventHandler(this.btn_about_Click);
            // 
            // lbl_hooked
            // 
            this.lbl_hooked.AutoSize = true;
            this.lbl_hooked.Location = new System.Drawing.Point(12, 41);
            this.lbl_hooked.Name = "lbl_hooked";
            this.lbl_hooked.Size = new System.Drawing.Size(105, 14);
            this.lbl_hooked.TabIndex = 11;
            this.lbl_hooked.Text = "Hook模式未启用";
            // 
            // list_down
            // 
            this.list_down.Activation = System.Windows.Forms.ItemActivation.OneClick;
            this.list_down.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.list_down.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.dURL,
            this.dProgress,
            this.dSize,
            this.dSetID,
            this.dName});
            this.list_down.ContextMenuStrip = this.menu_list;
            this.list_down.FullRowSelect = true;
            this.list_down.GridLines = true;
            this.list_down.Location = new System.Drawing.Point(12, 123);
            this.list_down.MultiSelect = false;
            this.list_down.Name = "list_down";
            this.list_down.Size = new System.Drawing.Size(478, 196);
            this.list_down.TabIndex = 13;
            this.list_down.UseCompatibleStateImageBehavior = false;
            this.list_down.View = System.Windows.Forms.View.Details;
            this.list_down.DoubleClick += new System.EventHandler(this.list_down_DoubleClick);
            // 
            // dURL
            // 
            this.dURL.Text = "下载URL";
            this.dURL.Width = 99;
            // 
            // dProgress
            // 
            this.dProgress.Text = "进度";
            this.dProgress.Width = 54;
            // 
            // dSize
            // 
            this.dSize.Text = "大小";
            this.dSize.Width = 59;
            // 
            // dSetID
            // 
            this.dSetID.Text = "SetID";
            this.dSetID.Width = 61;
            // 
            // dName
            // 
            this.dName.Text = "曲名";
            this.dName.Width = 323;
            // 
            // menu_list
            // 
            this.menu_list.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mStop,
            this.mRestart,
            this.mInfo,
            this.mCopy,
            this.mOgl,
            this.mYas});
            this.menu_list.Name = "menu_list";
            this.menu_list.Size = new System.Drawing.Size(233, 148);
            // 
            // mStop
            // 
            this.mStop.Name = "mStop";
            this.mStop.Size = new System.Drawing.Size(232, 24);
            this.mStop.Text = "停止下载";
            this.mStop.Click += new System.EventHandler(this.mStop_Click);
            // 
            // mRestart
            // 
            this.mRestart.Name = "mRestart";
            this.mRestart.Size = new System.Drawing.Size(232, 24);
            this.mRestart.Text = "重新下载";
            this.mRestart.ToolTipText = "如果多次重试失败，可以双击对应项调用浏览器下载 Orz";
            this.mRestart.Click += new System.EventHandler(this.mRestart_Click);
            // 
            // mInfo
            // 
            this.mInfo.Name = "mInfo";
            this.mInfo.Size = new System.Drawing.Size(232, 24);
            this.mInfo.Text = "打开谱面信息页";
            this.mInfo.ToolTipText = "打开osu官网对应的页面";
            this.mInfo.Click += new System.EventHandler(this.mInfo_Click);
            // 
            // mCopy
            // 
            this.mCopy.Name = "mCopy";
            this.mCopy.Size = new System.Drawing.Size(232, 24);
            this.mCopy.Text = "复制下载地址[bloodcat]";
            this.mCopy.ToolTipText = "复制完整的bloodcat下载链接";
            this.mCopy.Click += new System.EventHandler(this.mCopy_Click);
            // 
            // mOgl
            // 
            this.mOgl.Name = "mOgl";
            this.mOgl.Size = new System.Drawing.Size(232, 24);
            this.mOgl.Text = "从osu.uu.gl搜索该图";
            this.mOgl.ToolTipText = "由于不能按编号直接找到谱面，所以只能手动搜索";
            this.mOgl.Click += new System.EventHandler(this.mOgl_Click);
            // 
            // mYas
            // 
            this.mYas.Name = "mYas";
            this.mYas.Size = new System.Drawing.Size(232, 24);
            this.mYas.Text = "从yas-online搜索该图";
            this.mYas.ToolTipText = "由于下载地址可能不固定，所以只提供手动搜索";
            this.mYas.Click += new System.EventHandler(this.mYas_Click);
            // 
            // txt
            // 
            this.txt.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txt.Location = new System.Drawing.Point(12, 325);
            this.txt.Name = "txt";
            this.txt.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.txt.Size = new System.Drawing.Size(478, 156);
            this.txt.TabIndex = 17;
            this.txt.Text = "";
            this.txt.LinkClicked += new System.Windows.Forms.LinkClickedEventHandler(this.txt_LinkClicked);
            this.txt.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // mainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(566, 522);
            this.Controls.Add(this.btn_syncQQ);
            this.Controls.Add(this.txt);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel_setHook);
            this.Controls.Add(this.list_down);
            this.Controls.Add(this.btn_unhook);
            this.Controls.Add(this.lbl_hooked);
            this.Controls.Add(this.btn_hook);
            this.Controls.Add(this.btn_about);
            this.Controls.Add(this.chk_autoReturn);
            this.Controls.Add(this.rdo_auto);
            this.Controls.Add(this.rdo_manual);
            this.Controls.Add(this.rdo_disable);
            this.Controls.Add(this.lbl_path);
            this.Controls.Add(this.btn_Chooseosu);
            this.Controls.Add(this.pbr_download);
            this.Controls.Add(this.pic_direct);
            this.Name = "mainForm";
            this.Text = "Direct!osu";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.mainForm_FormClosing);
            this.Load += new System.EventHandler(this.mainForm_Load);
            this.panel_setHook.ResumeLayout(false);
            this.panel_setHook.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pic_direct)).EndInit();
            this.menu_list.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer timer1;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.NotifyIcon notifyIco;
        private System.Windows.Forms.ProgressBar pbr_download;
        private System.Windows.Forms.Button btn_Chooseosu;
        private System.Windows.Forms.Label lbl_path;
        private System.Windows.Forms.RadioButton rdo_disable;
        private System.Windows.Forms.RadioButton rdo_manual;
        private System.Windows.Forms.RadioButton rdo_auto;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.CheckBox chk_autoReturn;
        private System.Windows.Forms.PictureBox pic_direct;
        private System.Windows.Forms.Button btn_about;
        private System.Windows.Forms.Button btn_hook;
        private System.Windows.Forms.Label lbl_hooked;
        private System.Windows.Forms.Button btn_unhook;
        private System.Windows.Forms.ListView list_down;
        private System.Windows.Forms.ColumnHeader dSetID;
        private System.Windows.Forms.ColumnHeader dProgress;
        private System.Windows.Forms.ColumnHeader dName;
        private System.Windows.Forms.ColumnHeader dURL;
        private System.Windows.Forms.ColumnHeader dSize;
        private System.Windows.Forms.Panel panel_setHook;
        private System.Windows.Forms.RadioButton rdo_SEHook;
        private System.Windows.Forms.RadioButton rdo_CPHook;
        private System.Windows.Forms.ContextMenuStrip menu_list;
        private System.Windows.Forms.ToolStripMenuItem mStop;
        private System.Windows.Forms.ToolStripMenuItem mRestart;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton rdo_ALLHook;
        private System.Windows.Forms.RichTextBox txt;
        private System.Windows.Forms.ToolStripMenuItem mCopy;
        private System.Windows.Forms.ToolStripMenuItem mInfo;
        private System.Windows.Forms.Button btn_syncQQ;
        private System.Windows.Forms.ToolStripMenuItem mOgl;
        private System.Windows.Forms.ToolStripMenuItem mYas;
    }
}

