﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

namespace osu_Downloader
{
    public sealed class QQ
    {

        public static void Send2QQ(string text, string text2)
        {
            try
            {
                //if (Core._syncQQ)
                {
                    Type typeFromProgID = Type.GetTypeFromProgID("QQCPHelper.CPAdder");
                    object[] args = new object[]
					{
						text,
						65542,
						text2,
						""
					};
                    object target = Activator.CreateInstance(typeFromProgID);
                    typeFromProgID.InvokeMember("PutRSInfo", BindingFlags.InvokeMethod, null, target, args);
                }
            }
            catch
            {
                Console.WriteLine("Failed to Sync!");
            }
        }
    }
}
