﻿namespace osu_Downloader
{
    partial class About
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(About));
            this.link1 = new System.Windows.Forms.LinkLabel();
            this.link2 = new System.Windows.Forms.LinkLabel();
            this.SuspendLayout();
            // 
            // link1
            // 
            this.link1.AutoSize = true;
            this.link1.BackColor = System.Drawing.Color.Transparent;
            this.link1.Location = new System.Drawing.Point(12, 9);
            this.link1.Name = "link1";
            this.link1.Size = new System.Drawing.Size(147, 14);
            this.link1.TabIndex = 0;
            this.link1.TabStop = true;
            this.link1.Text = "wdwxy12345@gmail.com";
            this.link1.Click += new System.EventHandler(this.link2_Click);
            // 
            // link2
            // 
            this.link2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.link2.AutoSize = true;
            this.link2.BackColor = System.Drawing.Color.Transparent;
            this.link2.Location = new System.Drawing.Point(527, 9);
            this.link2.Name = "link2";
            this.link2.Size = new System.Drawing.Size(154, 14);
            this.link2.TabIndex = 1;
            this.link2.TabStop = true;
            this.link2.Text = "qq446015875@gmail.com";
            this.link2.Click += new System.EventHandler(this.link2_Click);
            // 
            // About
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(693, 357);
            this.Controls.Add(this.link2);
            this.Controls.Add(this.link1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "About";
            this.Opacity = 0.8D;
            this.Text = "direct!osu 感谢使用! v1.0 -2014.2";
            this.Load += new System.EventHandler(this.About_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.LinkLabel link1;
        private System.Windows.Forms.LinkLabel link2;
    }
}