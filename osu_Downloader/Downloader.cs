﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;

namespace osu_Downloader
{
    public class Downloader : IDisposable
    {
        private int _threadNum;             //线程数量
        private long _fileSize;             //文件大小
        private string _extName;            //文件扩展名
        private string _fileUrl;            //文件地址
        private string _fileName;           //文件名
        private string _savePath;           //保存路径
        private short _threadCompleteNum;   //线程完成数量
        private bool _isComplete;           //是否完成
        private volatile int _downloadSize; //当前下载大小
        private Thread[] _thread;           //线程数组
        private List<string> _tempFiles = new List<string>();
 
        public string FileName
        {
            get
            {
                return _fileName;
            }
            set
            {
                _fileName = value;
            }
        }

        public long FileSize
        {
            get
            {
                return _fileSize;
            }
        }

        public int DownloadSize
        {
            get
            {
                return _downloadSize;
            }
        }

        public bool IsComplete
        {
            get
            {
                return _isComplete;
            }
            set
            {
                _isComplete = value;
            }
        }

        public int ThreadNum
        {
            get
            {
                return _threadNum;
            }
            set
            {
                _threadNum = value;
            }
        }

        public string SavePath
        {
            get
            {
                return _savePath;
            }
            set
            {
                _savePath = value;
            }
        }
        public string ExtName
        {
            get
            {
                return _extName;
            }
            set
            {
                _extName = value;
            }
        }

        public Downloader(int threahNum, string fileUrl, string savePath)
        {
            this._threadNum = threahNum;
            this._thread = new Thread[threahNum];
            this._fileUrl = fileUrl;
            this._savePath = savePath;
        }
        public void Stop()
        {
            foreach (Thread th in _thread)
            {
                if (th != null)
                {
                    th.Abort();
                    th.Join();                    
                }
            }
        }
        public void Start()
        {
            HttpWebRequest request =
                (HttpWebRequest)WebRequest.Create(_fileUrl);
            request.Method = "GET";
            //request.KeepAlive = true;            
            request.UserAgent = "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; SV1; .NET CLR 1.1.4322; .NET CLR 2.0.50727)";
            HttpWebResponse response =
                (HttpWebResponse)request.GetResponse();
            if (_fileName == null)
            {
                string headers = response.Headers["Content-Disposition"];
                int headst = headers.IndexOf("filename=") + 1;
                int headend = headers.IndexOf(".osz", headst);
                if (headend == 0 || headst == 0)
                {
                    _fileName = _fileUrl.Remove(0, _fileUrl.LastIndexOf("/") + 1);
                }
                else
                {
                    _fileName = headers.Substring(headst + 9, headend - headst - 5);
                }
            }
            //获取真实扩展名
            if (_extName == null)
            {
                _extName = response.ResponseUri.ToString().Substring(response.ResponseUri.ToString().LastIndexOf('.'));
            }
            _fileSize = response.ContentLength;
            int singelNum = (int)(_fileSize / _threadNum);      //平均分配
            int remainder = (int)(_fileSize % _threadNum);      //获取剩余的
            request.Abort();
            response.Close();
            for (int i = 0; i < _threadNum; i++)
            {
                List<int> range = new List<int>();
                range.Add(i * singelNum);
                //剩余的交给最后一个线程
                if (remainder != 0 && (_threadNum - 1) == i)
                    range.Add(i * singelNum + singelNum +
                        remainder - 1);
                else
                    range.Add(i * singelNum + singelNum - 1);
                _thread[i] =
                    new Thread(() => { Download(range[0], range[1]); });
                _thread[i].Name = string.Format("directosu_temp_{0}", (i + 1));
                _thread[i].Start();
            }
        }

        private void Download(int from, int to)
        {
            Stream httpFileStream = null, localFileStram = null;
            try
            {
                string tmpFileBlock =
                    string.Format(@"{0}\{1}_{2}.dat", _savePath, _fileName, Thread.CurrentThread.Name);
                _tempFiles.Add(tmpFileBlock);
                HttpWebRequest httprequest =
                    (HttpWebRequest)WebRequest.Create(_fileUrl);
                httprequest.Method = "GET";                
                httprequest.KeepAlive = true;
                httprequest.UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.63 Safari/537.36";
                httprequest.AddRange(from, to);
                HttpWebResponse httpresponse =
                    (HttpWebResponse)httprequest.GetResponse();
                httpFileStream = httpresponse.GetResponseStream();
                localFileStram = new FileStream(tmpFileBlock, FileMode.Create);
                byte[] by = new byte[8000];
                //Read方法将返回读入by变量中的总字节数
                int getByteSize =
                    httpFileStream.Read(by, 0, (int)by.Length);
                while (getByteSize > 0)
                {
                    Thread.Sleep(20);
                    _downloadSize += getByteSize;
                    localFileStram.Write(by, 0, getByteSize);
                    getByteSize = httpFileStream.Read(by, 0, (int)by.Length);
                }
                _threadCompleteNum++;
            }
            catch (Exception ex)
            {
                //System.Windows.Forms.MessageBox.Show(ex.ToString());
                Console.WriteLine(ex.Message.ToString());
                if (System.Diagnostics.Process.GetCurrentProcess().MainWindowHandle == IntPtr.Zero)
                {
                    System.Windows.Forms.MessageBox.Show("osu已经关闭，正在结束direct!osu...","direct!exit");
                    System.Diagnostics.Process.GetCurrentProcess().Kill();
                }
                Thread.CurrentThread.Interrupt();
            }
            finally
            {
                if (httpFileStream != null) httpFileStream.Dispose();
                if (localFileStram != null) localFileStram.Dispose();
            }
            if (_threadCompleteNum == _threadNum)
            {
                _isComplete = true;
                Complete();
            }
        }

        private void Complete()
        {
            Stream mergeFile =
                new FileStream(
                    string.Format(@"{0}\{1}{2}", _savePath, _fileName, _extName), FileMode.Create);
            BinaryWriter AddWriter = new BinaryWriter(mergeFile);
            foreach (string file in _tempFiles)
            {
                using (FileStream fs = new FileStream(file, FileMode.Open))
                {
                    BinaryReader TempReader = new BinaryReader(fs);
                    AddWriter.Write(TempReader.ReadBytes((int)fs.Length));
                    TempReader.Close();
                }
                File.Delete(file);
            }
            AddWriter.Close();
        }

        public void Dispose()
        {
            this.Stop();
            GC.Collect();
        }
    }
}
