﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace osu_Downloader
{
    public partial class SyncToQQ : Form
    {
        public SyncToQQ()
        {
            InitializeComponent();
        }

        private void SyncToQQ_Load(object sender, EventArgs e)
        {
            txt_QQ.Text = Core._QQ;
            if (txt_QQ.Text == "")
            {
                txt_QQ.Text = Properties.Settings.Default._savedQQ;
                Core._QQ = Properties.Settings.Default._savedQQ;
            }
            chk_diff.Checked = Properties.Settings.Default._showDiff;
            chk_realName.Checked = Properties.Settings.Default._tryName;            
            if (Core._syncQQ)
            {
                this.Text = "direct!osu - QQ同步:当前正在同步";
            }
        }

        private void btn_cancel_Click(object sender, EventArgs e)
        {
            Core._syncQQ = false;
            Properties.Settings.Default._syncQQ = false;
            if (chk_save.Checked)
            {
                Properties.Settings.Default.Save();
            }
            QQ.Send2QQ(Core._QQ, "");
            this.Text = "direct!osu - QQ同步";
        }

        private void btn_accept_Click(object sender, EventArgs e)
        {
            long p;
            if (long.TryParse(txt_QQ.Text,out p))
            {
                Core._QQ = txt_QQ.Text;
                Properties.Settings.Default._savedQQ = Core._QQ;
                Properties.Settings.Default._syncQQ = true;
                Core._syncQQ = true;
                if (chk_save.Checked)
                {
                    Properties.Settings.Default.Save();
                }
                this.Text = "direct!osu - QQ同步:当前正在同步";
                this.Close();
            }
            else
            {
                MessageBox.Show("不能识别你的QQ号>o<","direct!error");
            }
        }

        private void chk_diff_CheckedChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default._showDiff = chk_diff.Checked;            
        }

        private void chk_realName_CheckedChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default._tryName = chk_realName.Checked;
        }
    }
}
